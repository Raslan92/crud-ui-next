# base image
FROM node:alpine
# create & set working directory for next app
RUN mkdir -p /usr/src
WORKDIR /usr/src
# copy source files next app
COPY . /usr/src
COPY package.json ./
# install dependencies for next app
RUN npm install
# start Next app
RUN npm run build
EXPOSE 3000
CMD npm run start
