import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import fetch from 'isomorphic-unfetch'
import React,{useState, useEffect} from 'react'
import axios from 'axios'


export default function Home() {

    const [post, setPost] = useState([])
    useEffect(()=> {
        var config = {
            headers: {'Access-Control-Allow-Origin': '*'}
        };
        axios.get('http://192.168.8.101:80/api/employee',config)
            .then(res =>{
                console.log(res.data)
                setPost(res.data)
            })
            .catch(err =>{
                console.log(err)
            })
    }, [])
    


  return (
    <div className={styles.container}>
      <Head>
        <meta charset="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Bootstrap Table with Add and Delete Row Feature</title>
        <link
            href="https://fonts.googleapis.com/css?family=Roboto"
            rel="stylesheet"  />
        <link
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet"
        />
        <link 
            href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            rel="stylesheet"  
        />
        <link 
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            rel="stylesheet" 
        />

      </Head>
      
      <body>
      
          <div className="container">
              <div className="table-wrapper">
                  <div className="table-title">
                      <div className="row">
                          <div className="col-sm-8"><h2>Employee <b>Details</b></h2></div>
                          <div className="col-sm-4">
                              <button type="button" className="btn btn-info add-new"><i className="fa fa-plus"></i> Add New</button>
                          </div>
                      </div>
                  </div>
                  <table className="table table-bordered">
                      <thead>
                          <tr>
                              <th>id</th>
                              <th>name</th>
                              <th>age</th>
                              {/* <th>Last logged in</th> */}
                          </tr>
                      </thead>
                      {post.map(post =>(
                      <tbody key={post.personid} >
                          <tr>
                              <td >{post.personid}</td>
                              <td>{post.names}</td>
                              <td>{post.age}</td>
                              {/* <td>{post.loggedin}</td> */}
                              <td>
                                  <a className="add" title="Add" data-toggle="tooltip"><i className="material-icons">&#xE03B;</i></a>
                                  <a className="edit" title="Edit" data-toggle="tooltip"><i className="material-icons">&#xE254;</i></a>
                                  <a className="delete" title="Delete" data-toggle="tooltip"><i className="material-icons">&#xE872;</i></a>
                              </td>
                          </tr>      
                      </tbody>
                      ))} 
                  </table>
              </div>
          </div>
      </body>
    </div>
  )
}